package com.puc.tcc.logistica.pedidoCarga.servicos.exception;

public class ServicosException extends Exception {
	
	public enum ServicosExceptionType {
		
	    INTERNO(1), VALIDACAO(2);
	 
	    public int codigo;
	    
	    ServicosExceptionType(int codigo) {
	    	this.codigo = codigo;
	    }
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 8539025235143825524L;
	
	private ServicosExceptionType servicosExceptionType;
	private String msg;
	
	@Deprecated
	public ServicosException(Exception e) {
		super(e);
	}
	
	@Deprecated
	public ServicosException(String e) {
		super(e);
	}
	
	public ServicosException(ServicosExceptionType servicosExceptionType, Exception e, String msg) {
		super(e);
		this.servicosExceptionType = servicosExceptionType;
		this.msg = msg;
	}
	
	public ServicosException(ServicosExceptionType servicosExceptionType, Exception e) {
		super(e);
		this.servicosExceptionType = servicosExceptionType;
	}
	
	public ServicosException(ServicosExceptionType servicosExceptionType, String msg) {
		super();
		this.servicosExceptionType = servicosExceptionType;
		this.msg = msg;
	}

	public ServicosExceptionType getServicosExceptionType() {
		return servicosExceptionType;
	}

	public void setServicosExceptionType(ServicosExceptionType servicosExceptionType) {
		this.servicosExceptionType = servicosExceptionType;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	

}
