package com.puc.tcc.logistica.pedidoCarga.dados;

public class DadosException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3435369908798078975L;
	
	public DadosException(Exception e) {
		super(e);
	}

}
