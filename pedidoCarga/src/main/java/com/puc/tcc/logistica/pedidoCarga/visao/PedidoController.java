package com.puc.tcc.logistica.pedidoCarga.visao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.puc.tcc.logistica.pedidoCarga.dominio.Pedido;
import com.puc.tcc.logistica.pedidoCarga.servicos.ServicosDePedido;
import com.puc.tcc.logistica.pedidoCarga.servicos.exception.ServicosException;
import com.puc.tcc.logistica.pedidoCarga.servicos.exception.ServicosException.ServicosExceptionType;
import com.puc.tcc.logistica.pedidoCarga.visao.exceptions.CorreiosException;
import com.puc.tcc.logistica.pedidoCarga.visao.exceptions.PedidoNotFoundException;
import com.puc.tcc.logistica.pedidoCarga.visao.exceptions.PedidoNotValidException;
 
@RestController
@RequestMapping("/pedido")
public class PedidoController {

	@Autowired
	private ServicosDePedido servicosDePedido;
	
	
	@GetMapping("/todas")
	ResponseEntity<List<Pedido>> all() {
		return ResponseEntity.status(HttpStatus.OK).body(servicosDePedido.getAll());
	}

	@PostMapping
	Pedido novoPedido(@RequestBody Pedido novoPedido, BindingResult result) {
		try {
		servicosDePedido.novoPedido(novoPedido);
		return novoPedido;
		} catch (ServicosException exception) {
			if (exception.getServicosExceptionType() == ServicosExceptionType.VALIDACAO)
				throw new PedidoNotValidException(exception.getMsg());
			else
				throw new CorreiosException(exception.getMsg());
		}
	}

	@GetMapping("/{id}")
	Pedido get(@PathVariable Long id) {
		Pedido pedido = servicosDePedido.get(id.longValue());
		if(pedido == null)
			throw new PedidoNotFoundException(id);
		else
			return pedido;
	}
	
	
}
