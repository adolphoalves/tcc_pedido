package com.puc.tcc.logistica.pedidoCarga.visao.adivices;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.puc.tcc.logistica.pedidoCarga.visao.exceptions.CorreiosException;

@ControllerAdvice
public class CorreiosAdivice {

	@ResponseBody
	@ExceptionHandler(CorreiosException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	String cargaNotFoundHandler(CorreiosException ex) {
		return ex.getMessage();
	}

}
