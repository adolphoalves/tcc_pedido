package com.puc.tcc.logistica.pedidoCarga.visao.adivices;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.puc.tcc.logistica.pedidoCarga.visao.exceptions.PedidoNotValidException;

@ControllerAdvice
public class PedidoNotFoundAdivice {

	@ResponseBody
	@ExceptionHandler(PedidoNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	String cargaNotFoundHandler(PedidoNotValidException ex) {
		return ex.getMessage();
	}

}
