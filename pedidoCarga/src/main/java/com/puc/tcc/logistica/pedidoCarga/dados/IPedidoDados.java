package com.puc.tcc.logistica.pedidoCarga.dados;

import org.springframework.data.repository.CrudRepository;

import com.puc.tcc.logistica.pedidoCarga.dominio.Pedido;

public interface IPedidoDados extends CrudRepository<Pedido, Long> {
	
}