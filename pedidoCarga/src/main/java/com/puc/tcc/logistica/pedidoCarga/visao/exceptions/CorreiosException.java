package com.puc.tcc.logistica.pedidoCarga.visao.exceptions;

public class CorreiosException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public CorreiosException(String error) {
		super(error);
	}
	
	

}
