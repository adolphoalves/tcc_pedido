package com.puc.tcc.logistica.pedidoCarga.servicos;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import java.lang.Iterable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.puc.tcc.logistica.pedidoCarga.dados.DadosException;
import com.puc.tcc.logistica.pedidoCarga.dados.IPedidoDados;
import com.puc.tcc.logistica.pedidoCarga.dominio.Pedido;
import com.puc.tcc.logistica.pedidoCarga.dominio.enums.StatusEnum;
import com.puc.tcc.logistica.pedidoCarga.servicos.exception.ServicosException;
import com.puc.tcc.logistica.pedidoCarga.servicos.exception.ServicosException.ServicosExceptionType;
import com.puc.tcc.logistica.pedidoCarga.webservices.correio.ServicoCorreio;

@Service
public class ServicosDePedido {
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	@Autowired
	private ServicoCorreio servicoCorreio;
	
	@Autowired
	private IPedidoDados pedidoDados;
	
	public void novoPedido(Pedido pedido)  throws ServicosException {

		validaPedido(pedido);
		try {
			pedido.setStatus(StatusEnum.NOVO);
			pedido.setDataPedido(new Date());
			pedidoDados.save(pedido);
			String json = new ObjectMapper().writeValueAsString(pedido);
		    kafkaTemplate.send("novoPedidoValidaTransferencia", json);
		    kafkaTemplate.send("novoPedidoHistorico", json);
		} catch (Exception e) {
			throw new ServicosException(ServicosExceptionType.INTERNO, e);
		}
	}
	
	public void validaPedido(Pedido pedido) throws ServicosException {
		
		try {
		
		StringBuilder erros = new StringBuilder();
		
		if(pedido == null)
			throw new ServicosException(ServicosExceptionType.VALIDACAO, "-dados_invalidos");

		if(pedido.getProfundidadeCaixa() <= 0 
				&& pedido.getAlturaCaixa() <= 0
				&& pedido.getLarguraCaixa() <= 0)
			erros.append("-dimenssoes_nao_informadas");

		if(pedido.getEnderecoOrigem() == null 
				|| !servicoCorreio.isCepValido(pedido.getEnderecoOrigem().getCep())
				|| pedido.getEnderecoOrigem().getNumero() <= 0)
			erros.append("-endereco_origem_invalido");

		if(pedido.getEnderecoDestino() == null 
				|| !servicoCorreio.isCepValido(pedido.getEnderecoDestino().getCep())
				|| pedido.getEnderecoDestino().getNumero() <= 0)
			erros.append("-endereco_destino_invalido");

		if(pedido.getIdCliente() <= 0)
			erros.append("-identificador_do_cliente_invalido");

		if(pedido.getPeso() <= 0)
			erros.append("-peso_nao_informado");
		
		if(erros.length() > 0)
			throw new ServicosException(ServicosExceptionType.VALIDACAO, erros.toString());
		
		} catch (DadosException e) {
			throw new ServicosException(ServicosExceptionType.INTERNO, e);
		}
	}
	
	public void atualiza(Pedido pedido) {
		try {
			pedidoDados.save(pedido);
			String json = new ObjectMapper().writeValueAsString(pedido);
		    kafkaTemplate.send("atualizacaoPedido", json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public Pedido get(long id) {
		return pedidoDados.findById(id).get();
	}
	
	public List<Pedido> getAll() {
		Iterable<Pedido> iPedidos = pedidoDados.findAll();
		List<Pedido> pedidos = new ArrayList<Pedido>();
		iPedidos.forEach(pedidos::add);
		return pedidos;
	}
	
	@KafkaListener(topics = "pedidoTransferido")
	public void transferePedido(ConsumerRecord<?, ?> consumerRecord) {
		try {
		    String mensagem = (String) consumerRecord.value();
		    ObjectNode node = new ObjectMapper().readValue(mensagem, ObjectNode.class);
		    int id = node.get("codigoPedido").intValue();
			Pedido pedido = get(id);
			pedido.setStatus(StatusEnum.TRANSFERIDO);
			atualiza(pedido);
		} catch (Exception e) {
			//TODO ERROR LAYER
		}
	}
	
}
