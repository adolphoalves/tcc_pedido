package com.puc.tcc.logistica.pedidoCarga.visao.exceptions;

public class PedidoNotValidException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PedidoNotValidException(String error) {
		super(error);
	}
	
	

}
