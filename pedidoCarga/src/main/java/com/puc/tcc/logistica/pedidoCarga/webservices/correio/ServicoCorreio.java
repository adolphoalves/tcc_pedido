package com.puc.tcc.logistica.pedidoCarga.webservices.correio;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.puc.tcc.logistica.pedidoCarga.dados.DadosException;
import com.puc.tcc.logistica.pedidoCarga.webservices.correio.servico.ConsultaCEPResponse;
import com.puc.tcc.logistica.pedidoCarga.webservices.correio.servico.EnderecoERP;
import com.puc.tcc.logistica.pedidoCarga.webservices.correio.servico.ConsultaCEP;

@Service
public class ServicoCorreio {
	
	@Autowired
	SOAPConnector soapConnector;
	
	public boolean isCepValido(int cep) throws DadosException {
		
		try {
			
			ConsultaCEP request = new ConsultaCEP();
			request.setCep(String.valueOf(cep));
			
			QName qName = new QName("http://cliente.bean.master.sigep.bsb.correios.com.br/", "consultaCEP");
	        JAXBElement<ConsultaCEP> root = (JAXBElement<ConsultaCEP>) new JAXBElement<>(qName, ConsultaCEP.class, request);

	        
	        @SuppressWarnings({"rawtypes" })
			JAXBElement jaxResponse = (JAXBElement) soapConnector.callWebService(
    				"https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente", root);
	        
	        ConsultaCEPResponse response = (ConsultaCEPResponse)jaxResponse.getValue();

	        if(response == null || response.getReturn() == null)
	        	return false;
	        
	        EnderecoERP endereco = response.getReturn();
		    boolean ehValido = (endereco != null && endereco.getEnd() != null && !endereco.getEnd().isEmpty());
			return ehValido;
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
//	public boolean isCepValido(int cep) throws DadosException {
//		
//		try {
//			AtendeClienteService servico = new AtendeClienteService();
//			AtendeCliente port = servico.getPort(AtendeCliente.class);
//			EnderecoERP endereco = port.consultaCEP(String.valueOf(cep));
//			return (endereco != null && endereco.getEnd() != null && !endereco.getEnd().isEmpty() );
//		}catch (SigepClienteException cepException) {
//			return false;
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw new DadosException(e);
//		}
//	}
	
}
